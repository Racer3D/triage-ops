# frozen_string_literal: true

require_relative File.expand_path('../lib/untriaged_issues_helper.rb', __dir__)

Gitlab::Triage::Resource::Context.include UntriagedIssuesHelper
