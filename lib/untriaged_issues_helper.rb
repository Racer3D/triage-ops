# frozen_string_literal: true

require_relative 'devops_labels'

module UntriagedIssuesHelper
  include UnlabelledIssuesHelper
  include DevopsLabels::Context

  TYPE_LABELS =
    %w(
      bug
      feature
      support\ request
      tooling
      meta
      documentation
      triage\ report
    ).freeze

  def untriaged?
    !triaged?
  end

  def triaged?
    has_type_label? && has_stage_label? && has_group_label?
  end

  def has_type_label?
    !type_label.nil?
  end

  def type_label
    (label_names & TYPE_LABELS).first
  end
end
