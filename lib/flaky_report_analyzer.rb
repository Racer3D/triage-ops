#!/usr/bin/env ruby

require 'json'
require 'date'

class FlakyReportAnalyzer
  attr_reader :report_file

  def initialize(report_file)
    unless report_file
      raise ArgumentError, 'Please provide a JSON flaky specs report file with the -f/--report-file option!'
    end

    @report_file = report_file
  end

  def print_report(group_by: nil, limit: nil)
    group_by ||= :file
    limit ||= 1000

    report = JSON.parse(File.read(File.expand_path(report_file)))

    case group_by
    when :file, :reports
      report_by_file = report.each_with_object(Hash.new { 0 }) do |(uid, hash), memo|
        memo[hash['file']] += group_by == :reports ? hash['flaky_reports'] : 1
      end

      report_by_file = report_by_file.sort_by { |_, count| count }.reverse.first(limit).reverse
      report_by_file.reverse! if small_limit?(limit)

      report_by_file.map { |file, count| "#{file}: #{count}" }.join("\n")
    when :type
      report_by_type = report.each_with_object(Hash.new { 0 }) do |(uid, hash), memo|
        file_parts = hash['file'].split('/')
        type = file_parts[2] == 'ee' ? file_parts[4] : file_parts[2]
        memo[type] += 1
      end

      report_by_type = report_by_type.sort_by { |_, count| count }.reverse.first(limit).reverse
      report_by_type.reverse! if small_limit?(limit)

      report_by_type.map { |file, count| "#{file}: #{count}" }.join("\n")
    when :date
      report_by_date = report.each_with_object({}) do |(uid, hash), memo|
        memo[Date.parse(hash['last_flaky_at']).to_s] ||= []
        memo[Date.parse(hash['last_flaky_at']).to_s] << hash['file']
      end

      report_by_date = report_by_date.sort_by { |date, _| date }.reverse.first(limit).reverse
      report_by_date.reverse! if small_limit?(limit)

      report_by_date.map { |date, files| "#{date}: #{files.size} files\n\t#{files})" }.join("\n")
    else
      raise ArgumentError, 'Unknown group-by strategy: only :file and :date are supported'
    end
  end

  def small_limit?(limit)
    limit <= 25
  end
end
