# frozen_string_literal: true

module Generate
  Option = Struct.new(:template, keyword_init: true) do
    def self.parse(argv)
      require 'optparse'

      options = new

      OptionParser.new do |parser|
        parser.banner = "Usage: #{$PROGRAM_NAME} [options]\n\n"

        parser.on('-t', '--template TEMPLATE', String, 'Path to the template') do |value|
          options.template = value
        end

        parser.on('-h', '--help', 'Print help message') do
          $stdout.puts parser
          exit
        end
      end.parse!(argv)

      options
    end
  end
end
