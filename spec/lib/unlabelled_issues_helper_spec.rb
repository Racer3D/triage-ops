# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/unlabelled_issues_helper'

RSpec.describe UnlabelledIssuesHelper do
  let(:resource_klass) do
    Struct.new(:issue) do
      include UnlabelledIssuesHelper
    end
  end

  subject { resource_klass.new }

  let(:list_items) { (1..24).to_a.map { |i| "Item ##{i}"} }
  let(:potential_triagers) { %w[@triager-1 @triager-2 @triager-3 @triager-4 @triager-5] }

  before do
    stub_const('UnlabelledIssuesHelper::POTENTIAL_TRIAGERS', potential_triagers)
  end

  describe '#distribute_items' do
    it 'distributes items all items across triagers' do
      distribution = subject.distribute_items(list_items)
      items = distribution.values.inject(&:+)

      expect(items).to match_array(list_items)
    end

    it 'sorts triagers by username' do
      distribution = subject.distribute_items(list_items)
      triagers = distribution.keys

      expect(triagers).to eq(potential_triagers)
    end
  end
end
