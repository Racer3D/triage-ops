# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/devops_labels'

RSpec.describe DevopsLabels do
  describe '.sections' do
    it 'returns the sections' do
      sections = described_class.sections

      expect(sections).to include('dev', 'ops')
    end
  end

  describe '.section_labels' do
    it 'returns the section labels' do
      labels = described_class.section_labels

      expect(labels).to include('section::dev', 'section::ops')
    end
  end

  describe '.stages' do
    it 'returns the stages' do
      stages = described_class.stages

      expect(stages).to include('manage', 'enablement')
    end
  end

  describe '.stage_labels' do
    it 'returns the stage labels' do
      labels = described_class.stage_labels

      expect(labels).to include('devops::manage', 'devops::enablement')
    end
  end

  describe '.groups_per_stage' do
    it 'returns a { stage => groups } hash' do
      groups_per_stage = described_class.groups_per_stage

      expect(groups_per_stage.keys).to match_array(described_class.stages)
    end
  end

  describe '.categories_per_group' do
    it 'returns a { group => categories } hash' do
      categories_per_group = described_class.categories_per_group

      expect(categories_per_group.keys).to match_array(described_class.groups)
    end
  end

  describe '.groups' do
    it 'returns the groups' do
      groups = described_class.groups

      expect(groups).to include('access', 'ecosystem')
    end
  end
  describe '.stage_and_departments_labels' do
    it 'returns the stage labels' do
      labels = described_class.stage_and_departments_labels

      expect(labels).to include('devops::manage', 'Quality')
    end
  end

  describe '.group_labels' do
    it 'returns the group labels' do
      labels = described_class.group_labels

      expect(labels).to include('group::access', 'group::ecosystem')
    end
  end

  describe '.category_labels' do
    it 'returns the category labels' do
      labels = described_class.category_labels

      expect(labels).to include('Category:Audit Reports', 'static analysis')
    end
  end

  describe '.deprecated_team_labels' do
    it 'returns the team labels' do
      labels = described_class.deprecated_team_labels

      expect(labels.first).to eq('Manage [DEPRECATED]')
    end
  end

  describe '.department_labels' do
    it 'returns the department labels' do
      labels = described_class.department_labels

      expect(labels.first).to eq('Quality')
    end
  end

  describe '.department?' do
    it 'returns true when given a department' do
      expect(described_class.department?('Quality')).to eq(true)
    end

    it 'returns true when given a department' do
      expect(described_class.department?('Foo')).to eq(false)
    end
  end

  describe '.teams_per_user' do
    where(:username, :expected_teams) do
      [
        ['andr3', ['source code']],
        ['felipe_artur', ['certify', 'portfolio management']],
        ['luke', []],
        ['jramsay', ['dev']],
        ['non-gitlab-user', []]
      ]
    end

    with_them do
      it 'returns teams of user' do
        expect(described_class.teams_per_user(username)).to contain_exactly(*expected_teams)
      end
    end
  end

  describe DevopsLabels::Context do
    let(:resource_klass) do
      Struct.new(:labels) do
        include DevopsLabels::Context
      end
    end
    let(:label_klass) do
      Struct.new(:name)
    end
    let(:resource) { resource_klass.new([]) }

    describe '#label_names' do
      it 'returns [] if the resource has no label' do
        resource = resource_klass.new([])

        expect(resource.label_names).to eq([])
      end

      it 'returns the label names if the resource has labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.label_names).to eq([DevopsLabels.stage_and_departments_labels.first])
      end
    end

    describe '#current_stage_label' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the stage label if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.current_stage_label).to eq(DevopsLabels.stage_and_departments_labels.first)
      end

      it 'returns nil if the resource has an unknown stage label' do
        resource = resource_klass.new([label_klass.new('devops::foo bar baz')])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns nil if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains')])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the department label if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource.current_stage_label).to eq('Quality')
      end

      it 'returns the department label if the resource has a department label and an unknown stage label' do
        resource = resource_klass.new([label_klass.new('Quality'), label_klass.new('devops::foo bar baz')])

        expect(resource.current_stage_label).to eq('Quality')
      end

      it 'returns the stage label if the resource has a stage label and a nested "sub-stage" label' do
        first_stage_label = DevopsLabels.stage_and_departments_labels[0]
        second_stage_label = DevopsLabels.stage_and_departments_labels[1]
        resource = resource_klass.new([label_klass.new("#{first_stage_label}::merge trains"), label_klass.new(second_stage_label)])

        expect(resource.current_stage_label).to eq(second_stage_label)
      end
    end

    describe '#current_group_label' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_label).to be_nil
      end

      it 'returns the group label if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_label).to eq(DevopsLabels.group_labels.first)
      end

      it 'returns nil if the resource has an unknown group label' do
        resource = resource_klass.new([label_klass.new('group::foo bar baz')])

        expect(resource.current_group_label).to be_nil
      end
    end

    describe '#current_infrastructure_team_label' do
      it 'returns nil if the resource has no infrastructure team label' do
        resource = resource_klass.new([])

        expect(resource.current_infrastructure_team_label).to be_nil
      end

      it 'returns the group label if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource.current_infrastructure_team_label).to eq('team::Scalability')
      end
    end

    describe 'current_department_label' do
      it 'returns nil if the resource has no department label' do
        resource = resource_klass.new([])

        expect(resource.current_department_label).to be_nil
      end

      it 'returns the department label if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource.current_department_label).to eq('Quality')
      end
    end

    describe '#current_category_labels' do
      it 'returns [] if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource.current_category_labels).to eq([])
      end

      it 'returns the category labels if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.current_category_labels).to eq([DevopsLabels.category_labels.first])
      end
    end

    describe '#current_deprecated_team_label' do
      it 'returns nil if the resource has no team label' do
        resource = resource_klass.new([])

        expect(resource.current_deprecated_team_label).to be_nil
      end

      it 'returns the team name if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.deprecated_team_labels.first)])

        expect(resource.current_deprecated_team_label).to eq(DevopsLabels.deprecated_team_labels.first)
      end
    end

    describe '#current_stage_name' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.current_stage_name).to eq(DevopsLabels.stages.first)
      end

      it 'returns nil if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains')])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label and a nested "sub-stage" label' do
        first_stage_label = DevopsLabels.stage_and_departments_labels[0]
        second_stage_label = DevopsLabels.stage_and_departments_labels[1]
        second_stage = DevopsLabels.stages[1]
        resource = resource_klass.new([label_klass.new("#{first_stage_label}::merge trains"), label_klass.new(second_stage_label)])

        expect(resource.current_stage_name).to eq(second_stage)
      end
    end

    describe '#current_group_name' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_name).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_name).to eq(DevopsLabels.groups.first)
      end
    end

    describe '#all_category_labels_for_stage' do
      it 'returns [] if the stage does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a stage name' do
        expect(resource.all_category_labels_for_stage('release')).to include('Category:Continuous Delivery', 'Category:Review Apps')
        expect(resource.all_category_labels_for_stage('release')).not_to include('logging')
      end
    end

    describe '#all_category_labels_for_group' do
      it 'returns [] if the group does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a group name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group('progressive delivery')).to include('Category:Continuous Delivery')
        expect(resource.all_category_labels_for_group('progressive delivery')).not_to include('release governance')
      end
    end

    describe '#section_for_stage' do
      it 'returns the section for the specific stage' do
        resource = resource_klass.new([])

        expect(resource.section_for_stage('create')).to eq('dev')
        expect(resource.section_for_stage('verify')).to eq('ops')
      end
    end

    describe '#section_for_group' do
      it 'returns the section for the specific group' do
        resource = resource_klass.new([])

        expect(resource.section_for_group('source code')).to eq('dev')
        expect(resource.section_for_group('memory')).to eq('enablement')
      end
    end

    describe '#has_stage_label?' do
      it 'returns false if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_stage_label
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource).to be_has_stage_label
      end

      it 'returns false if the resource has a nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains')])

        expect(resource).not_to be_has_stage_label
      end
    end

    describe '#has_group_label?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_group_label
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource).to be_has_group_label
      end
    end

    describe '#can_add_default_group?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group
      end

      it 'returns false if the resource has an group label' do
        resource = resource_klass.new([label_klass.new('group::access')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns false if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource).not_to be_can_add_default_group
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::manage')])

        expect(resource).to be_can_add_default_group
      end
    end

    describe '#can_add_default_group_and_stage?' do
      it 'returns true if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a group label' do
        resource = resource_klass.new([label_klass.new('group::access')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has an infrastructure team label' do
        resource = resource_klass.new([label_klass.new('team::Scalability')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a department label' do
        resource = resource_klass.new([label_klass.new('Quality')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end

      it 'returns false if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::manage')])

        expect(resource).not_to be_can_add_default_group_and_stage
      end
    end

    describe '#has_category_label?' do
      it 'returns false if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource).not_to have_category_label
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource).to have_category_label
      end
    end

    describe '#has_category_label_for_current_stage?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_stage
      end

      it 'returns false if the resource has a stage label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('logging')])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label, a corresponding category label, and an unrelated nested "sub-stage" label' do
        resource = resource_klass.new([label_klass.new('devops::release::merge trains'), label_klass.new('devops::verify'), label_klass.new('Category:Merge Trains')])

        expect(resource).to be_has_category_label_for_current_stage
      end
    end

    describe '#has_category_label_for_current_group?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_group
      end

      it 'returns true if the resource has a group label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_group
      end

      it 'returns false if the resource has a group label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('release governance')])

        expect(resource).not_to be_has_category_label_for_current_group
      end
    end

    describe '#can_infer_labels?' do
      it 'returns false if the resource has no stage, group, category, or team label' do
        resource = resource_klass.new([])

        expect(resource.can_infer_labels?).to eq(false)
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.deprecated_team_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end
    end

    describe '#can_infer_group_from_author?' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end
      let(:author) { 'johndoe' }
      let(:resource) { resource_klass.new(author) }

      before do
        allow(resource).to receive(:group_for_user).with(author)
      end

      it 'returns true if there is a matching group for user' do
        allow(resource).to receive(:group_for_user).and_return(true)

        expect(resource.can_infer_group_from_author?).to eq(true)
      end

      it 'returns false if there is no matching group for user' do
        allow(resource).to receive(:group_for_user).and_return(nil)

        expect(resource.can_infer_group_from_author?).to eq(false)
      end
    end

    describe '#single_deprecated_team_label? & #can_infer_from_deprecated_team_label?' do
      it 'returns false if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).not_to be_single_deprecated_team_label
        expect(resource).not_to be_can_infer_from_deprecated_team_label
      end

      it 'returns true if the resource has a single team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.deprecated_team_labels.first)])

        expect(resource).to be_single_deprecated_team_label
        expect(resource).to be_can_infer_from_deprecated_team_label
      end

      it 'returns true if the resource has several team labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.deprecated_team_labels.first), label_klass.new(DevopsLabels.deprecated_team_labels.last)])

        expect(resource).not_to be_single_deprecated_team_label
        expect(resource).not_to be_can_infer_from_deprecated_team_label
      end
    end

    describe '#stage_has_a_single_group?' do
      it 'returns false if the stage does not exists' do
        expect(resource.stage_has_a_single_group?(nil)).to be(false)
        expect(resource.stage_has_a_single_group?('')).to be(false)
        expect(resource.stage_has_a_single_group?('foo')).to be(false)
      end

      it 'returns true if the give stage has a single group' do
        expect(resource.stage_has_a_single_group?('package')).to be(true)
      end

      it 'returns false if the resource has several team labels' do
        expect(resource.stage_has_a_single_group?('create')).to be(false)
      end
    end

    describe '#group_part_of_stage?' do
      let(:stage_label) { 'devops::create' }
      let(:group_label) { 'group::source code' }
      let(:category_label) { 'Category:Code Review' }
      let(:current_labels) { [stage_label, group_label] }
      let(:resource) { resource_klass.new(current_labels.map { |l| label_klass.new(l) }) }

      context 'when stage and group do not exist' do
        let(:current_labels) { [] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when stage does not exist' do
        let(:current_labels) { [group_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group does not exist' do
        let(:current_labels) { [stage_label] }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is not part of stage' do
        let(:stage_label) { 'devops::configure' }
        let(:group_label) { 'group::continuous integration' }

        it 'returns false' do
          expect(resource.group_part_of_stage?).to be(false)
        end
      end

      context 'when group is part of stage' do
        it 'returns true' do
          expect(resource.group_part_of_stage?).to be(true)
        end
      end
    end

    describe '#first_group_for_stage' do
      it 'returns false if the stage does not exists' do
        expect(resource.first_group_for_stage(nil)).to be_nil
        expect(resource.first_group_for_stage('')).to be_nil
        expect(resource.first_group_for_stage('foo')).to be_nil
      end

      it 'returns the first group of the given stage' do
        expect(resource.first_group_for_stage('enablement')).to eq('distribution')
      end
    end

    describe '#stage_for_group' do
      it 'returns nil if the no stage corresponds to the given group' do
        expect(resource.stage_for_group(nil)).to be_nil
        expect(resource.stage_for_group('')).to be_nil
        expect(resource.stage_for_group('foo')).to be_nil
      end

      it 'returns the stage name of the given group' do
        expect(resource.stage_for_group('distribution')).to eq('enablement')
      end
    end

    describe '#inference_strategy_for_issue' do
      let(:stage_label) { 'devops::create' }
      let(:group_label) { 'group::source code' }
      let(:category_label) { 'Category:Code Review' }
      let(:current_labels) { [] }
      let(:resource) { resource_klass.new(current_labels.map { |l| label_klass.new(l) }) }

      subject { resource.inference_strategy_for_issue }

      context 'when stage: yes, group: yes, category: yes' do
        let(:current_labels) { [stage_label, group_label, category_label] }

        context 'when category is part of stage' do
          it { is_expected.to be_nil }
        end

        context 'when category is not part of stage' do
          let(:category_label) { 'Category:Wiki' }

          it { is_expected.to eq :infer_category_from_group }
        end
      end

      context 'when stage: yes, group: yes, category: no' do
        let(:current_labels) { [stage_label, group_label] }

        it { is_expected.to eq :infer_category_from_group }
      end

      context 'when stage: yes, group: no, category: yes' do
        let(:current_labels) { [stage_label, category_label] }

        it { is_expected.to eq :infer_group_from_category }
      end

      context 'when stage: yes, group: no, category: no' do
        let(:current_labels) { [stage_label] }

        # TODO: This looks odd, review this. :infer_group_from_category might do more than inferring from category
        it { is_expected.to eq :infer_group_from_category }
      end

      context 'when stage: no, group: yes, category: yes' do
        let(:current_labels) { [group_label, category_label] }

        it { is_expected.to eq :infer_stage_and_category_from_group }
      end

      context 'when stage: no, group: yes, category: no' do
        let(:current_labels) { [group_label] }

        it { is_expected.to eq :infer_stage_and_category_from_group }
      end

      context 'when stage: no, group: no, category: yes' do
        let(:current_labels) { [category_label] }

        it { is_expected.to eq :infer_stage_and_group_from_category }
      end
    end

    describe '#inference_strategy_for_merge_request' do
      let(:stage_label) { 'devops::create' }
      let(:group_label) { 'group::source code' }
      let(:category_label) { 'Category:Code Review' }
      let(:current_labels) { [] }
      let(:resource) { resource_klass.new(current_labels.map { |l| label_klass.new(l) }) }

      subject { resource.inference_strategy_for_merge_request }

      context 'when stage: yes, group: yes, category: yes' do
        let(:current_labels) { [stage_label, group_label, category_label] }

        it { is_expected.to be_nil }
      end

      context 'when stage: yes, group: yes, category: no' do
        let(:current_labels) { [stage_label, group_label] }

        context 'when group is part of stage' do
          let(:stage_label) { 'devops::create' }
          let(:group_label) { 'group::source code' }

          it { is_expected.to eq :infer_category_from_group }
        end

        context 'when group is not part of stage' do
          let(:stage_label) { 'devops::create' }
          let(:group_label) { 'group::continuous integration' }

          it { is_expected.to be_nil }
        end
      end

      context 'when stage: yes, group: no, category: yes' do
        let(:current_labels) { [stage_label, category_label] }

        it { is_expected.to eq :infer_group_from_stage }
      end

      context 'when stage: yes, group: no, category: no' do
        let(:current_labels) { [stage_label] }

        it { is_expected.to eq :infer_group_from_stage }
      end

      context 'when stage: no, group: yes, category: yes' do
        let(:current_labels) { [group_label, category_label] }

        it { is_expected.to be_nil }
      end

      context 'when stage: no, group: yes, category: no' do
        let(:current_labels) { [group_label] }

        it { is_expected.to be_nil }
      end

      context 'when stage: no, group: no, category: yes' do
        let(:current_labels) { [category_label] }

        it { is_expected.to be_nil }
      end
    end

    describe '#new_stage_and_group_labels_from_intelligent_inference and #comment_for_intelligent_stage_and_group_labels_inference' do
      where(:case_name, :infer_from_category, :current_labels, :expected_new_stage_and_group_labels_from_intelligent_inference, :explanation) do
        [
          ["stage: yes, group: yes, category: yes, team: no => Only section.", true, ["devops::configure", "group::configure", "Category:Wiki"], ["section::ops"], ''],
          ["stage: yes, group: yes, category: yes, team: no, no category inference => Only section.", false, ["devops::configure", "group::configure", "Category:Wiki"], ["section::ops"], ''],

          ["stage: yes, group: yes, category: no, team: no => Only section.", true, ["devops::configure", "group::configure"], ["section::ops"], ''],
          ["stage: yes, group: yes, category: no, team: no, no category inference => Only section.", false, ["devops::configure", "group::configure"], ["section::ops"], ''],
          ["stage: yes, group: yes, category: no, team: no, category inference => override for Navigation", false, ["devops::growth", "group::expansion"], ["section::growth"], ''],

          ["stage: yes, group: no, category: yes, team: no (100% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "Category:Wiki"], ["group::knowledge", "section::dev"], %(Setting label(s) ~"group::knowledge" ~"section::dev" based on ~"Category:Wiki" ~"group::knowledge".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups) => Manual triage required", true, ["devops::create", "Category:Wiki", "merge requests"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "Category:Wiki", "Category:Design Management", "analytics"], ["group::knowledge", "section::dev"], %(Setting label(s) ~"group::knowledge" ~"section::dev" based on ~"Category:Design Management" ~"Category:Wiki" ~"group::knowledge".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "Category:Wiki", "analytics"], ["group::knowledge", "section::dev"], %(Setting label(s) ~"group::knowledge" ~"section::dev" based on ~"Category:Wiki" ~"group::knowledge".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "Category:Design Management", "analytics"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "Category:Wiki", "analytics", "epics"], ["group::knowledge", "section::dev"], %(Setting label(s) ~"group::knowledge" ~"section::dev" based on ~"Category:Wiki" ~"group::knowledge".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Manual triage required since feature is less than 50%", true, ["devops::create", "search", "analytics", "epics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "Category:Design Management", "analytics", "epics"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Manual triage required since feature does not match stage but stage has several groups", true, ["devops::secure", "Category:Design Management"], ["section::securedefend"], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "Category:Design Management"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on category since group has only one category", true, ["devops::create", "Category:Gitaly"], ["group::gitaly", "section::dev"], %(Setting label(s) ~"group::gitaly" ~"section::dev" based on ~"Category:Gitaly" ~"group::gitaly".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage), no category inference => Manual triage required", false, ["devops::create", "Category:Wiki"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups), no category inference => Manual triage required", false, ["devops::create", "Category:Wiki", "merge requests"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage), no category inference => Manual triage required", false, ["devops::create", "Category:Wiki", "Category:Design Management", "analytics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage), no category inference => Manual triage required", false, ["devops::create", "Category:Wiki", "analytics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "Category:Design Management", "analytics"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Manual triage required", false, ["devops::create", "Category:Wiki", "analytics", "epics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Manual triage required since feature is less than 50%", false, ["devops::create", "search", "analytics", "epics"], ["section::dev"], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "Category:Design Management", "analytics", "epics"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Manual triage required since feature does not match stage but stage has several groups", false, ["devops::secure", "Category:Design Management"], ["section::securedefend"], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Manual triage required since feature does not match stage but stage has several groups", false, ["devops::secure", "Category:Design Management"], ["section::securedefend"], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "Category:Design Management"], ["group::package", "section::ops"], %(Setting label(s) ~"group::package" ~"section::ops" based on ~"devops::package" ~"group::package".)],

          ["stage: yes, group: no, category: no, team: no => Manual triage required", true, ["devops::verify"], ["section::ops"], ''],
          ["stage: yes, group: no, category: no, team: no, no category inference => Manual triage required", false, ["devops::verify"], ["section::ops"], ''],

          ["stage: no, group: yes, category: yes, team: no => Stage based on group", true, ["Category:Design Management", "group::source code", "markdown"], ["devops::create", "section::dev"], %(Setting label(s) ~"devops::create" ~"section::dev" based on ~"group::source code".)],

          ["stage: no, group: yes, category: no, team: no => Stage label based on group", true, ["group::memory"], ["devops::enablement", "section::enablement"], %(Setting label(s) ~"devops::enablement" ~"section::enablement" based on ~"group::memory".)],

          ["stage: no, group: no, category: no, team: yes => Stage based on team", true, ["Plan [DEPRECATED]", "backend", "bug"], ["devops::plan", "section::dev"], %(Setting label(s) ~"devops::plan" ~"section::dev" based on ~"Plan [DEPRECATED]" ~"devops::plan".)],
          ["stage: no, group: no, category: no, team: no => Manual triage required", true, ["bug", "rake tasks"], [], ''],
          ["stage: no, group: no, category: no, team: yes, no category inference => Stage based on team", false, ["Plan [DEPRECATED]", "backend", "bug"], ["devops::plan", "section::dev"], %(Setting label(s) ~"devops::plan" ~"section::dev" based on ~"Plan [DEPRECATED]" ~"devops::plan".)],
          ["stage: no, group: no, category: no, team: no, no category inference => Manual triage required", false, ["bug", "rake tasks"], [], ''],

          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on feature", true, ["Category:Internationalization"], ["devops::manage", "group::import", "section::dev"], %(Setting label(s) ~"devops::manage" ~"group::import" ~"section::dev" based on ~"Category:Internationalization" ~"group::import".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on category", true, ["Category:Source Code Management"], ["devops::create", "group::source code", "section::dev"], %(Setting label(s) ~"devops::create" ~"group::source code" ~"section::dev" based on ~"Category:Source Code Management" ~"group::source code".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%) => Stage and group based on feature", true, ["Category:Snippets", "Category:Web IDE", "Category:Internationalization"], ["devops::create", "group::editor", "section::dev"], %(Setting label(s) ~"devops::create" ~"group::editor" ~"section::dev" based on ~"Category:Web IDE" ~"Category:Snippets" ~"group::editor".)],
          ["stage: no, group: no, category: yes, team: no (best match: 50%) => Manual triage required", true, ["Category:Snippets", "Category:Web IDE", "Category:Live Preview", "Category:Importers", "Category:Internationalization", "Category:Templates"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 33%) => Manual triage required", true, ["pipeline", "elasticsearch", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (no match) => Manual triage required", true, ["backstage"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 100%), no category inference => Manual triage required", false, ["Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 100%), no category inference => Manual triage required", false, ["Category:Source Code Management"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 66%), no category inference => Manual triage required", false, ["Category:Snippets", "Category:Web IDE", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 50%), no category inference => Manual triage required", false, ["elasticsearch", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 33%), no category inference => Manual triage required", false, ["pipeline", "elasticsearch", "Category:Internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (no match), no category inference => Manual triage required", false, ["backstage"], [], ''],

          ["~QA => ~Quality", true, ["QA"], ["Quality"], %(Setting label(s) ~"Quality" based on ~"QA".)],
          ["~Quality ~QA => No new labels", true, ["Quality", "QA"], [], ''],
          ["~Quality ~group::package => Only section", true, ["Quality", "group::package"], ["section::ops"], ''],
          ["~QA => ~Quality, no category inference", false, ["QA"], [], ''],
          ["~Quality ~QA, no category inference => No new labels", false, ["Quality", "QA"], [], ''],
          ["~Quality ~group::package => Only section", false, ["Quality", "group::package"], ["section::ops"], ''],

          ["~insights => ~Engineering Productivity", true, ["insights"], ["Engineering Productivity"], %(Setting label(s) ~"Engineering Productivity" based on ~"insights".)],
          ["~Engineering Productivity ~insights => No new labels", true, ["Engineering Productivity", "insights"], [], ''],
          ["~insights => ~Engineering Productivity, no category inference", false, ["insights"], [], ''],
          ["~Engineering Productivity ~insights, no category inference => No new labels", false, ["Engineering Productivity", "insights"], [], ''],

          ["stage: yes, group: yes, category: no, single_category: yes => Apply Category label based on group", true, ["group::gitaly", "devops::create"], ["Category:Gitaly", "section::dev"], %(Setting label(s) ~"Category:Gitaly" ~"section::dev" based on ~"group::gitaly".)],
          ["stage: no, group: yes, category: no, single_category: yes => Apply Stage and Category label based on group", true, ["group::gitaly"], ["devops::create", "Category:Gitaly", "section::dev"], %(Setting label(s) ~"devops::create" ~"Category:Gitaly" ~"section::dev" based on ~"group::gitaly".)],
          ["stage: no, group: yes, category: no, single_category: no => Apply Stage based on group, cannot infer Category", true, ["group::geo"], ["devops::enablement", "section::enablement"], %(Setting label(s) ~"devops::enablement" ~"section::enablement" based on ~"group::geo".)],
          ["stage: yes, group: yes, category: no, single_category: no => Stage and Group already applied, cannot infer Category based on group. Do nothing", true, ["group::geo", "devops::enablement"], ["section::enablement"], ''],

          ["stage: yes, group: yes, category: no, group_part_of_stage: yes => Apply category from group if possible", false, ["devops::create", "group::gitter"], ["Category:Gitter", "section::dev"], %(Setting label(s) ~"Category:Gitter" ~"section::dev" based on ~"group::gitter".)],
          ["stage: yes, group: yes, category: no, group_part_of_stage: yes => Do not apply category from group if there are multiple categories", false, ["devops::verify", "group::testing"], ["section::ops"], ''],
          ["stage: yes, group: yes, category: no => Do nothing - cannot infer from stage, too many categories per stage", false, ["devops::verify", "group::configure"], ["section::ops"], ''],
          ["stage: no, group: yes, category: yes, legacy_team: no => Only section", false, ["Category:Design Management", "group::source code"], ["section::dev"], ''],
          ["stage: no, group: yes, category: no, team: no => Only section", false, ["group::memory"], ["section::enablement"], ''],

          ["stage: no, group: yes, category: yes => Apply stage and group labels to triage report", true, ["triage report", "group::access"], ["devops::manage", "section::dev"], %(Setting label(s) ~"devops::manage" ~"section::dev" based on ~"group::access".)]
        ]
      end

      it "returns nil if the resource does not warrant any new labels" do
        resource = resource_klass.new([])

        expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
      end

      with_them do
        it "returns a comment with a /label quick action" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })
          label_action = "/label #{expected_new_stage_and_group_labels_from_intelligent_inference.map { |l| %(~"#{l}") }.join(' ')}"

          if expected_new_stage_and_group_labels_from_intelligent_inference.empty?
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: infer_from_category)).to be_nil
          elsif expected_new_stage_and_group_labels_from_intelligent_inference.all? { |label| label.start_with?('section::') }
            # When there's only section labels added, be quiet without explanation
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: infer_from_category)).to eq(label_action)
          else
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: infer_from_category)).to eq %(#{explanation}\n#{label_action})
          end
        end
      end
    end

    describe '#group_for_user' do
      it 'returns nil when given bad username' do
        expect(resource.group_for_user(nil)).to be_nil
        expect(resource.group_for_user('')).to be_nil
      end

      it 'returns the group name of the matching group for user' do
        expect(resource.group_for_user('andr3')).to eq('source code')
      end

      it 'returns nil when user has multiple groups' do
        expect(resource.group_for_user('felipe_artur')).to be_nil
      end

      it 'returns nil when team refers to stage' do
        expect(resource.group_for_user('jramsay')).to be_nil
      end
    end

    describe '#comment_for_mr_author_group_label' do
      let(:resource_klass) do
        Struct.new(:author) do
          include DevopsLabels::Context
        end
      end
      let(:resource) { resource_klass.new(author) }

      context 'when author belongs to one group' do
        let(:author) { 'andr3' }

        it 'returns comment with a /label quick action' do
          expected_label = '~"group::source code"'
          expected_comment = "Setting label #{expected_label} based on `@#{author}`'s group.\n/label #{expected_label}"

          expect(resource.comment_for_mr_author_group_label).to eq(expected_comment)
        end
      end

      context 'when author belongs to multiple groups' do
        let(:author) { 'felipe_artur' }

        it 'returns nil' do
          expect(resource.comment_for_mr_author_group_label).to be_nil
        end
      end
    end
  end
end
