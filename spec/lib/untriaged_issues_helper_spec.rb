# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/untriaged_issues_helper'

RSpec.describe UntriagedIssuesHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include UntriagedIssuesHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  subject { resource_klass.new(labels) }

  describe '#triaged?' do
    context 'for triaged resource' do
      let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify'), label_klass.new('bug')] }

      it 'returns true' do
        expect(subject.triaged?).to eq(true)
      end
    end

    context 'for untriaged resource' do
      context 'no stage label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('bug')] }

        it 'returns false' do
          expect(subject.triaged?).to eq(false)
        end
      end

      context 'no group label' do
        let(:labels) { [label_klass.new('devops::verify'), label_klass.new('bug')] }

        it 'returns false' do
          expect(subject.triaged?).to eq(false)
        end
      end

      context 'no type label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify')] }

        it 'returns false' do
          expect(subject.triaged?).to eq(false)
        end
      end
    end

    context 'for unlabelled resource' do
      it 'returns false' do
        expect(subject.triaged?).to eq(false)
      end
    end
  end
end
